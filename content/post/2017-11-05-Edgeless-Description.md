---
title: What is Edgeless
subtitle: Basic stats
date: 2017-11-05
---

Edgeless is a coin that uses Ethereum token to run an online casino.

* Blackjack is the only game that works right now
    - Not fully functional yet

* Claims:
  1. Casino based on blockchain
  2. No house edge: players have a fair statistical chance of breaking even
		- Fairer to players
		- Gets $ from player mistakes: many players will lose money from mistakes
  3. No withdrawal fees

Crowd sale in March.
Launched Sept. 28
Blackjack is the only game available now

* Statistics (in late October 2017)
 - Market cap: $76M
 - Supply: ~132 million EDG
 - Value: $0.57 per token
 - 3769 total token holders

* Social media (in late October 2017)
    - Reddit: 1,100 followers
    - Twitter: 18,700 followers
    - Blog on Medium